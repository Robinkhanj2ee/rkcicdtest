FROM openjdk:17-alpine
EXPOSE 8080
ADD target/spring-docker-rk.jar spring-docker-rk.jar

# java -jar /opt/app/app.jar
ENTRYPOINT ["java","-jar","/spring-docker-rk.jar"]
