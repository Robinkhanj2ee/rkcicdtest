package com.example.cicd.rkcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RkcicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(RkcicdApplication.class, args);
	}

}
