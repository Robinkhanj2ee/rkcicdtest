package com.example.cicd.rkcicd;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RkController {
	
	@GetMapping("/v1")
	public String getMsg() {
		return "Hello Rk world";
	}

}
